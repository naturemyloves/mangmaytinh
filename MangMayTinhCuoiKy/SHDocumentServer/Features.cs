﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SHDocumentServer
{
    public class Features
    {
        public static readonly string OnlineStatus = "Online";
        public static readonly string OfflineStatus = "Offline";
        public bool LogOn(string userName, string passWord)
        {
            var user = XmlHelper.GetUser(userName);
            if (user == null)
                return false;
            else
            {
                var result = user.Password.Equals(PasswordHelper.HashPassword(passWord));
                if (result)
                {
                    user.CurrentIp = Features.GetLocalIPAddress();
                    user.Status = OnlineStatus;
                    XmlHelper.UpdateUser(user);
                }
                return result;
            }
        }

        public bool SignUp(string userName, string passWord)
        {
            return false;
        }

        public void TransferData()
        {
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
