﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Xml.Linq;

namespace SHDocumentServer
{
    public static class XmlHelper
    {
        public static readonly string filePath = "~/Data/Data.xml";
        public static readonly string idNode = "id";
        public static readonly string userNameNode = "username";
        public static readonly string passWordNode = "password";
        public static readonly string statusNode = "status";
        public static readonly string currentIpNode = "currentip";
        public static readonly string usersNode = "users";
        public static readonly string userNode = "user";

        public static User GetUser(string userName)
        {
            try
            {
                var path = System.IO.Path.GetFullPath(filePath);
                var xmlFile = XDocument.Load(path);
                var query = from w in xmlFile.Descendants(userNode)
                            where w.Element(userNameNode).Value.ToLower().Equals(userName.ToLower())
                            select new User
                            {
                                Id = w.Element(idNode) == null ? string.Empty : w.Element(idNode).Value,
                                UserName = w.Element(userNameNode) == null ? string.Empty : w.Element(userNameNode).Value,
                                Password = w.Element(passWordNode) == null ? string.Empty : w.Element(userNameNode).Value,
                                Status = w.Element(statusNode) == null ? string.Empty : w.Element(userNameNode).Value,
                                CurrentIp = w.Element(currentIpNode) == null ? string.Empty : w.Element(userNameNode).Value
                            };
                return query.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void UpdateUser(User user)
        {
            try
            {
                var path = System.IO.Path.GetFullPath(filePath);
                var xmlFile = XDocument.Load(path);
                var target = xmlFile.Element(usersNode).Elements(userNode).Where(e => e.Element(userNameNode).Value.Equals(user.UserName, StringComparison.CurrentCultureIgnoreCase)).Single();
                if (target == null)
                    return;
                target.Element(statusNode).Value = user.Status;
                target.Element(currentIpNode).Value = user.CurrentIp;
                xmlFile.Save(path);
            }
            catch (Exception ex)
            {
                return;
            }
        }
        public static void CreateUser(User user)
        {
            try
            {
                var path = System.IO.Path.GetFullPath(filePath);
                var xmlFile = XDocument.Load(path);
                var target = xmlFile.Element(usersNode).Elements(userNode).Where(e => e.Element(userNameNode).Value.Equals(user.UserName, StringComparison.CurrentCultureIgnoreCase)).Single();
                if (target != null)
                    return;
                var newUserElement = new XElement(userNode);
                newUserElement.Add(new XElement(idNode, user.Id));
                newUserElement.Add(new XElement(userNameNode, user.UserName.ToLower()));
                newUserElement.Add(new XElement(passWordNode, PasswordHelper.HashPassword(user.Password)));
                newUserElement.Add(new XElement(statusNode, user.Status));
                newUserElement.Add(new XElement(currentIpNode, user.CurrentIp));
                xmlFile.Element(usersNode).Add(newUserElement);
                xmlFile.Save(path);
            }
            catch (Exception ex)
            {
                return;
            }
        }

    }
}
