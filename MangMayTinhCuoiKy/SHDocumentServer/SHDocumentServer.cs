﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SHDocumentServer
{
    public partial class SHDocumentServer : Form
    {
        public static List<string> messages = new List<string>();
        public static List<Thread> clientConnection = new List<Thread>();
        public static List<TcpConnection> clientList = new List<TcpConnection>();
        private const int port = 12345;
        public static Dictionary<string, string> listData;
        delegate void SetTextCallback(string text);
        TcpListener listener;
        TcpClient client;
        NetworkStream ns;
        Thread t = null;
        List<TcpClient> listConnectedClients;
        public SHDocumentServer()
        {
            InitializeComponent();

        }
        public void DoWork()
        {
            byte[] bytes = new byte[1024];
            while (true)
            {
                int bytesRead = ns.Read(bytes, 0, bytes.Length);
                this.SetText(Encoding.ASCII.GetString(bytes, 0, bytesRead));
            }
        }

        private void SetText(string text)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.listBox1.Items.Add(text);
            }
        }


        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void SHDocumentServer_Load(object sender, EventArgs e)
        {


        }

        private string[] HandleReceivedData(string data)
        {
            string[] result;
            result = data.Split(',');
            return result;
        }

        private string ProcessData(string data)
        {
            var raw = HandleReceivedData(data);
            var index = 0;
            switch (raw.Length)
            {
                case 1:
                    if (Int32.TryParse(raw[0], out index))
                        return GetAllFile();
                    break;
                case 2:
                    if (Int32.TryParse(raw[0], out index))
                        return GetAllFile(raw[1]);
                    break;
                case 3:
                    if (Int32.TryParse(raw[0], out index))
                        return GetAllFile(raw[1]);
                    break;
                default:
                    return "Disconnected";
            }
            return "empty";
        }

        private string GetAllFile(string searchText)
        {
            var result = string.Empty;
            listData = listData == null ? new Dictionary<string, string>() : listData;
            var lines = File.ReadAllLines("./Data/Documents.txt");
            foreach (var line in lines)
            {
                var record = line.Split(',');
                listData.Add(record[0], record[1]);
                if (record[1].ToLower().Contains(searchText.ToLower()))
                    result += record[1];
            }
            return result;
        }

        private string GetAllFile()
        {
            var result = string.Empty;
            listData = listData == null ? new Dictionary<string, string>() : listData;
            var lines = File.ReadAllLines("./Data/Documents.txt");
            foreach (var line in lines)
            {
                var record = line.Split(',');
                listData.Add(record[0], record[1]);
                result += record[1];
            }
            return result;
        }

        private void button_StartServer_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 port = 13000;
                listener = new TcpListener(IPAddress.Any, port);
                listener.Start();
                listConnectedClients = new List<TcpClient>();
                Byte[] bytes = new Byte[1048 * 100];
                String data = null;
                while (true)
                {
                    listBox1.Items.Add("Waiting for a connection... ");
                    TcpClient client = listener.AcceptTcpClient();
                    listConnectedClients.Add(client);
                    listBox1.Items.Add("Connected!");
                    data = null;
                    NetworkStream stream = client.GetStream();
                    int i;
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        data = Encoding.ASCII.GetString(bytes, 0, i);
                        var reponse = ProcessData(data);
                        listBox1.Items.Add($"Received: {data}");
                        byte[] msg = Encoding.ASCII.GetBytes(reponse);
                        stream.Write(msg, 0, msg.Length);
                        listBox1.Items.Add($"Sent: {data}");
                    }
                    client.Close();
                }
            }

            catch (SocketException ex)
            {
                listBox1.Items.Add($"SocketException: {ex}");
            }
            finally
            {
                listener.Stop();
            }
        }

        
    }
}
