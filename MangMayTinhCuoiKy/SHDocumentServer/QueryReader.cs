﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace SHDocumentServer
{
    public class QueryReader
    {
        List<String> messages;
        List<TcpConnection> listenerList;
        Boolean successed;
        Mutex mutex;
        public QueryReader(List<String> query, List<TcpConnection> listeners, Mutex mut)
        {
            messages = query;
            listenerList = listeners;
            mutex = mut;
            successed = false;
        }

        public void Run()
        {
            while (!successed)
            {
                mutex.WaitOne();
                for (int i = 0; i < listenerList.Count; i++)
                {
                    if (listenerList[i].Connected == false)
                    {
                        listenerList.RemoveAt(i);
                    }
                }

                if (messages.Count > 0)
                {
                    foreach (TcpConnection listener in listenerList)
                    {
                        listener.SendMessage(messages[0]);
                    }
                    messages.RemoveAt(0);
                    Thread.Sleep(10);
                }
                mutex.ReleaseMutex();
            }
        }
    }
}
