﻿namespace SHDocumentServer
{
    partial class SHDocumentServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage_NhatKyHeThong = new System.Windows.Forms.TabPage();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.tabPage_DanhSachNguoiDung = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colUserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCurrentIp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_StartServer = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage_NhatKyHeThong.SuspendLayout();
            this.tabPage_DanhSachNguoiDung.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage_NhatKyHeThong);
            this.tabControl1.Controls.Add(this.tabPage_DanhSachNguoiDung);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(937, 477);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage_NhatKyHeThong
            // 
            this.tabPage_NhatKyHeThong.Controls.Add(this.listBox1);
            this.tabPage_NhatKyHeThong.Location = new System.Drawing.Point(4, 22);
            this.tabPage_NhatKyHeThong.Name = "tabPage_NhatKyHeThong";
            this.tabPage_NhatKyHeThong.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_NhatKyHeThong.Size = new System.Drawing.Size(929, 451);
            this.tabPage_NhatKyHeThong.TabIndex = 1;
            this.tabPage_NhatKyHeThong.Text = "Nhật ký hệ thống";
            this.tabPage_NhatKyHeThong.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox1.Enabled = false;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(0, 2);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(929, 433);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // tabPage_DanhSachNguoiDung
            // 
            this.tabPage_DanhSachNguoiDung.Controls.Add(this.dataGridView1);
            this.tabPage_DanhSachNguoiDung.Location = new System.Drawing.Point(4, 22);
            this.tabPage_DanhSachNguoiDung.Name = "tabPage_DanhSachNguoiDung";
            this.tabPage_DanhSachNguoiDung.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_DanhSachNguoiDung.Size = new System.Drawing.Size(894, 340);
            this.tabPage_DanhSachNguoiDung.TabIndex = 0;
            this.tabPage_DanhSachNguoiDung.Text = "Danh sách người dùng";
            this.tabPage_DanhSachNguoiDung.UseVisualStyleBackColor = true;
            this.tabPage_DanhSachNguoiDung.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId,
            this.colUserName,
            this.colStatus,
            this.colCurrentIp});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(888, 334);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // colId
            // 
            this.colId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colId.HeaderText = "Id";
            this.colId.MinimumWidth = 100;
            this.colId.Name = "colId";
            this.colId.ReadOnly = true;
            // 
            // colUserName
            // 
            this.colUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.colUserName.HeaderText = "UserName";
            this.colUserName.MinimumWidth = 250;
            this.colUserName.Name = "colUserName";
            this.colUserName.ReadOnly = true;
            this.colUserName.Width = 250;
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.MinimumWidth = 100;
            this.colStatus.Name = "colStatus";
            this.colStatus.ReadOnly = true;
            this.colStatus.Width = 200;
            // 
            // colCurrentIp
            // 
            this.colCurrentIp.HeaderText = "Current IP Adress";
            this.colCurrentIp.MinimumWidth = 150;
            this.colCurrentIp.Name = "colCurrentIp";
            this.colCurrentIp.ReadOnly = true;
            this.colCurrentIp.Width = 200;
            // 
            // button_StartServer
            // 
            this.button_StartServer.Location = new System.Drawing.Point(8, 483);
            this.button_StartServer.Name = "button_StartServer";
            this.button_StartServer.Size = new System.Drawing.Size(925, 81);
            this.button_StartServer.TabIndex = 2;
            this.button_StartServer.Text = "Start Server";
            this.button_StartServer.UseVisualStyleBackColor = true;
            this.button_StartServer.Click += new System.EventHandler(this.button_StartServer_Click);
            // 
            // SHDocumentServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 572);
            this.Controls.Add(this.button_StartServer);
            this.Controls.Add(this.tabControl1);
            this.Name = "SHDocumentServer";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Text = "SHDocumentServer";
            this.Load += new System.EventHandler(this.SHDocumentServer_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage_NhatKyHeThong.ResumeLayout(false);
            this.tabPage_DanhSachNguoiDung.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage_DanhSachNguoiDung;
        private System.Windows.Forms.TabPage tabPage_NhatKyHeThong;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId;
        private System.Windows.Forms.DataGridViewTextBoxColumn colUserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn colCurrentIp;
        private System.Windows.Forms.Button button_StartServer;
    }
}

