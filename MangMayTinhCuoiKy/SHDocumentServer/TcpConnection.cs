﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SHDocumentServer
{
    public class TcpConnection
    {
        public Socket Handler { get; set; }
        public List<string> Messages { get; set; }
        public Mutex Mutex { get; set; }
        public bool Connected { get { return Handler.Connected; } }
        public TcpConnection(Socket socket, List<String> query, Mutex mut)
        {
            Handler = socket;
            Messages = query;
            Mutex = mut;
        }
        private string ReceivedMessage()
        {
            byte[] buffer = new byte[4048];
            int bytesReceived = Handler.Receive(buffer);
            String str = Encoding.ASCII.GetString(buffer, 0, bytesReceived);
            return str;
        }


        public void SendMessage(string data)
        {
            byte[] msg = Encoding.ASCII.GetBytes(DateTime.Now + " " + data);
            Handler.Send(msg);
        }


        public void Run()
        {
            string message;
            while (Handler.Connected)
            {
                try
                {
                    message = ReceivedMessage();
                    if (!string.IsNullOrWhiteSpace(message))
                    {
                        Mutex.WaitOne();
                        Messages.Add(message);
                        Mutex.ReleaseMutex();
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }
    }
}

