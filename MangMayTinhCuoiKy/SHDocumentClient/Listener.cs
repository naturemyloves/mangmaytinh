﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SHDocumentClient
{
    public class Listener
    {
        public List<string> Messages { get; set; }
        public Socket Handler { get; set; }
        public bool Connected { get { return Handler.Connected; } }
        public Listener(Socket socket, List<string> stringList)
        {
            Handler = socket;
            Messages = stringList;
        }

        public void Run()
        {
            while (Handler.Connected)
            {
                try
                {
                    byte[] bytes = new Byte[4048];
                    int bytesReceived = Handler.Receive(bytes);
                    Messages.Add(Encoding.ASCII.GetString(bytes, 0, bytesReceived));
                }
                catch (SocketException ex)
                {

                }
            }
        }
    }
}
