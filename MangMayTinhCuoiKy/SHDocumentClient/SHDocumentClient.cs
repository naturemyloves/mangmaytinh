﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SHDocumentClient
{
    public partial class SHDocumentClient : Form
    {
        public int port;
        public string userName = "Khoa";
        public string passWord = "Khoa";
        public Socket sender;
        public Listener listener;
        public Thread thread;
        public string server;
        delegate void SetTextCallback(string text);

        TcpClient client;
        NetworkStream ns;
        Thread t = null;
        private const string hostName = "localhost";

        List<string> messageQuery = new List<String>();

        public SHDocumentClient()
        {
            InitializeComponent();
            port = 13000;
            server = "127.0.0.1";
        }


        #region Define Event
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void SHDocumentClient_Load(object sender, EventArgs e)
        {
            try
            {
                client = new TcpClient(server, port);
                Byte[] data = Encoding.ASCII.GetBytes(userName);
                NetworkStream stream = client.GetStream();
                stream.Write(data, 0, data.Length);
                listBox1.Items.Add($"Sent:{userName}");
                data = new Byte[1048];
                String responseData = String.Empty;
                Int32 bytes = stream.Read(data, 0, data.Length);
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
                listBox1.Items.Add($"Received: {responseData}");
                stream.Close();
                client.Close();
            }
            catch (ArgumentNullException ex)
            {
                listBox1.Items.Add($"ArgumentNullException: {ex}");
            }
            catch (SocketException ex)
            {
                listBox1.Items.Add($"SocketException: {ex}");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var index = comboBox1.SelectedIndex;
            SendRequest(index.ToString());
            if (index == 4)
            {
                client.Close();
            }

        }

        private void SendRequest(string request)
        {
            client = new TcpClient(server, port);
            Byte[] data = Encoding.ASCII.GetBytes(request);
            NetworkStream stream = client.GetStream();
            stream.Write(data, 0, data.Length);
            listBox1.Items.Add($"Sent: {request}");
            data = new Byte[1048 * 100];
            String responseData = String.Empty;
            Int32 bytes = stream.Read(data, 0, data.Length);
            responseData = Encoding.ASCII.GetString(data, 0, bytes);
            listBox1.Items.Add("Response: ");
            listBox1.Items.Add($"\n {responseData}");
            listBox1.Items.Add("End Response: ");
            stream.Close();
            client.Close();
        }

        private string HandleSelection(int index)
        {

            if (index == 1 || index == 2)
                return $"{index},{textBox1.Text}";
            else
                return $"{index}";
        }
        #endregion End event

    }
}
